# Formation kafka (Découverte)

## Explorer les outils kafka

### Exercice 1: Créer un topic
1. Lancer kafka via docker
    ```bash
    docker-compose up -d
    ```
1. Se connecter au broker par docker
    ```bash
   docker exec -it formation-kafka_kafka_1 bash
   ```
1. Créer un topic **MonTopic**
    ```bash
   kafka-topics --create --bootstrap-server localhost:9092 --topic MonTopic
   ```
   Explorer la commande `kafka-topics`

### Exercice 2: Ecrire/Lire dans un topic (console) 
1. Se connecter au broker par docker
1. Ecrire dans le topic **MonTopic** avec un kafka console producer
    ```bash
   kafka-console-producer --help
    ```
1. Lire le topic **MonTopic** avec un kafka console consumer
    ```bash
   kafka-console-consumer --help
   ```
1. Explorer le topic via Kafdrop
    * Lancer Kafdrop [http://localhost:9000](http://localhost:9000)

### Exercice 3: Contrôler les consumers groups 
1. Via la console regarder les consumers groups
    ```bash
   kafka-consumer-groups --help 
   ```
1. Explorer les consumers groups via Kafdrop

## Use case : Station météo

Une station météo fournit des relevés de température en temps réel. 
Notre application va traiter les données pour calculer la température moyenne par intervalle de temps.

### Story 1: Ecrire/Lire les relevés de température dans un topic
Il n'existe pas de connecteur entre notre station météo et Kafka. 
Le relevé de température de la station météo sera simulé par un fichier.

Format du fichier
>mot 12bit en hexa<br/>
>exemple:<br/>
>0A5<br/>
>1A5


Tâche:
1. Ecrire un producer qui va lire ce fichier et écrire dans le topic **raw-temperature**.
    ```java
   var producer = new KafkaProducer(...);
   producer.send(...);
    ``` 

### Story 2: Kafka streams (operation stateless)
Les données sont brutes, il faut les rendre exploitable. Pour cela, nous allons
utiliser kafka-streams pour convertir les messages brutes du topic en message au format Json.

>Format Json<br/>
> ```json
> {
>  "temp": 12.5 
> }
>```
>
>Utiliser la méthode de conversion du capteur (TemperatureApi.convert)

Tâche:
1. Création d'un test
    ```java
   var testDriver = new TopologyTestDriver(...);
   var input = testDriver.createInputTopic(...);
   var output = testDriver.createOutputTopic(...);
   
   // Send
   input.pipeInput(...);
   // Read
   output.readValue();
   
   // Assert
    ```
1. Création d'un topology
    ```java
   var builder = new StreamsBuilder();
   // use builder for streams operation
   Topology topology = builder.build(); 
   ```
1. Suppression des températures négatives
1. Transformation de la donnée brute simple vers le format JSON dans le topic **json-temperature**

> Utilisation des operations stateless de kafkastreams `map`, `filter`, `mapValues`...

### Story 3: Kafka streams (operation statefull)
Garder la température maxi par seconde

Tâche:
1. Calculer la température maxi par seconde.
