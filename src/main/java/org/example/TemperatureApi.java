package org.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class TemperatureApi {

    public static List<String> getTemperature() {
        try {
            return Files.readAllLines(Paths.get("src/main/resources/temperature.raw"));
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    private static float resolution = 40.95f;

    public static float convert(String value) {
        var toInt = Integer.parseInt(value, 16);
        return toInt / resolution - 50f;
    }
}
