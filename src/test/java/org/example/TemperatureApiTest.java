package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TemperatureApiTest {

    @Test
    public void should_returns_50_when_FFF() {
        // Given
        var hexa = "FFF";
        // When
        float res = TemperatureApi.convert(hexa);
        // Then
        assertEquals(50f, res);
    }

    @Test
    public void should_returns_50_negative_when_0() {
        // Given
        var hexa = "000";
        // When
        float res = TemperatureApi.convert(hexa);
        // Then
        assertEquals(-50f, res);
    }

    @Test
    public void should_returns_temp_negative_when_12f() {
        // Given
        var hexa = "12F";
        // When
        float res = TemperatureApi.convert(hexa);
        // Then
        assertEquals(-42.6f, res, 0.01f);
    }

}